from dotenv import load_dotenv
load_dotenv()

from os import environ, walk, path
import sys, docker, re
from docker.errors import APIError, ImageNotFound, NotFound

client = docker.from_env()

# PDFs to parse
PDFS_DIR = '0-pdfs/'
get_filenames_from_dir = lambda dir_path: next(walk(dir_path))[2]
ALL_PDFS = get_filenames_from_dir(PDFS_DIR)
# (_, _, ALL_PDFS) = next(walk(PDFS_DIR))
PDF_PATH = PDFS_DIR + '{pdf}'
PDFS_PATH = expand(PDF_PATH, pdf=ALL_PDFS)

# Extracted contents from PDFs
remove_extension = lambda file_path: path.splitext(file_path)[0]
RAW_CONTENTS_DIR = '1-contents/'
RAW_CONTENT = RAW_CONTENTS_DIR + '{name}_raw_contents.{ext}'
JSONS_RAW_CONTENTS = expand(RAW_CONTENT, name=[remove_extension(name) for name in ALL_PDFS], ext=['json'] )
XML_RAW_CONTENTS = expand(RAW_CONTENT, name=[remove_extension(name) for name in ALL_PDFS], ext=['xml'] )

# Extracted articles from contents
# INPUT_CONTENTS = [file_path for file_path in get_filenames_from_dir(RAW_CONTENTS_DIR) if file_path.endswith('.xml')]
EXTRACTED_ARTICLES_DIR = '2-articles/'
EXTRACTED_ARTICLE = EXTRACTED_ARTICLES_DIR + '{name}_articles.json'
EXTRACTED_ARTICLE_PATHS = expand(EXTRACTED_ARTICLE, name=[name.replace('.pdf','') for name in ALL_PDFS])

# Extracted concepts from articles
EXTRACTED_CONCEPTS_DIR = '3-concepts/'
EXTRACTED_CONCEPT = EXTRACTED_CONCEPTS_DIR + '{name}_concepts.json'
EXTRACTED_CONCEPTS_PATHS = expand(EXTRACTED_CONCEPT, name=[name.replace('.pdf','') for name in ALL_PDFS])

# Extracted visualizations from concepts
EXTRACTED_VISUAL_DIR = '4-visualizations/'
EXTRACTED_VISUAL = EXTRACTED_VISUAL_DIR + '{name}_visual_{type}.json'
EXTRACTED_VISUAL_WC_PATHS = expand(EXTRACTED_VISUAL, name=[name.replace('.pdf','') for name in ALL_PDFS], type='word_count')

# Indexed results
INDEXED_DATA = 'index.json'

def start_docker_container():
    detach = True
    ports = {'9998/tcp': '9998'}
    try:
        container = client.containers.run(
            environ.get('TIKA_CONTAINER'),
            detach=detach,
            ports=ports,
            name=environ.get('TIKA_CONTAINER_NAME')
        )
    except ImageNotFound as err:
        print('Cannot find {} container...'.format(environ.get('TIKA_CONTAINER')))
        raise err
    except APIError as err:
        if err.status_code == 409:
            container = client.containers.get(environ.get('TIKA_CONTAINER_NAME'))
            container.restart()
        else:
            raise
    except:
        print('Unhandled error: ', sys.exc_info()[0])
        raise

    print('Waiting for container to be up and runnging...')
    # no timeout if no match occur (waiting indefinitely)
    for log in container.logs(stream=True, tail=0, timestamps=True):
        print('LOG FROM CONTAINER: ' + log.decode())
        if re.search(environ.get('TIKA_COONTAINER_STARTED_STRING'), log.decode()):
            break

    return

def stop_docker_container():
    try:
        container = client.containers.get(environ.get('TIKA_CONTAINER_NAME'))
        container.stop()
    except NotFound as err:
        print('Cannot find {} to stop.'.format(environ.get('TIKA_CONTAINER_NAME')))
        raise err
    except:
        print('Unhandled error: ', sys.exc_info()[0])
        raise
    return

onstart:
    print('Starting required docker container...')
    start_docker_container()
    print('Container started successfully!')

onsuccess:
    print('Stopping running docker container...')
    stop_docker_container()
    print('Container stopped successfully.')

onerror:
    print('Stopping running docker container...')
    stop_docker_container()
    print('Container stopped successfully.')

rule all:
    input: JSONS_RAW_CONTENTS,
            XML_RAW_CONTENTS,
            EXTRACTED_ARTICLE_PATHS,
            EXTRACTED_CONCEPTS_PATHS,
            EXTRACTED_VISUAL_WC_PATHS

rule extract_text_content:
    input: PDFS_PATH
    output: JSONS_RAW_CONTENTS, XML_RAW_CONTENTS
    script: 'scripts/extract_from_pdf.py'

# rule extract_text_with_layouts:
#     input: PDFS_PATH[0]
#     output: XML_RAW_CONTENTS[0] + '_w_layout.xml'
#     shell: 'python scripts/tools/pdf2txt.py --output_type xml -S -o {output} {input}'

rule rearrange_as_articles:
    input: XML_RAW_CONTENTS
    output: EXTRACTED_ARTICLE_PATHS
    script: 'scripts/content_to_articles.py'

rule extract_concepts:
    input: EXTRACTED_ARTICLE_PATHS
    output: EXTRACTED_CONCEPTS_PATHS
    script: 'scripts/article_to_concepts.py'

rule extract_counts:
    input: EXTRACTED_CONCEPTS_PATHS
    output: EXTRACTED_VISUAL_WC_PATHS
    script: 'scripts/tokens_to_visualizations.py'
