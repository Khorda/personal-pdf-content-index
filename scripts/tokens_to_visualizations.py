import json
import sys
from collections import Counter


def _lower_tokens(tokens):
    return [t.lower() for t in tokens]


class WordCloudVisualization:
    def __init__(self, data):
        self.data = data
        self.tokens_per_page = [page['tokens'] for page in self.data]
        self.counted = []
        self._tokens_to_counted()

    @classmethod
    def from_json(cls, raw_json):
        return cls(raw_json)

    def _tokens_to_counted(self):
        for token_list in self.tokens_per_page:
            self.counted.append(Counter(_lower_tokens(token_list)))

    def _aggregate_count(self):
        all_counter = Counter()
        for counter in self.counted:
            all_counter |= counter
        return all_counter

    def write(self, fp=sys.stdout):
        fp.write('[')
        for pg_i, counted_page in enumerate(self.counted):
            json.dump([{'text': cntd_tkn[0], 'value': cntd_tkn[1]} for cntd_tkn in counted_page.most_common()], fp,
                      ensure_ascii=False)
            if pg_i < len(self.counted) - 1:
                fp.write(',')
        fp.write(']')

    def write_aggregated(self, fp=sys.stdout):
        json.dump([{'text': cntd_tkn[0], 'value': cntd_tkn[1]} for cntd_tkn in self._aggregate_count().most_common()],
                  fp, ensure_ascii=False)


def main(from_snakemake=True):
    if from_snakemake is False:
        inputs = [sys.argv[i] for i in range(1, len(sys.argv))]
    else:
        inputs = snakemake.input

    word_counts = []
    for input_path in inputs:
        with open(input_path) as fp:
            raw = json.load(fp)
        word_counts.append(WordCloudVisualization.from_json(raw))

    if from_snakemake is False:
        outputs = [sys.stdout for _ in range(0, len(inputs))]
    else:
        outputs = [open(snakemake.output[path_index], 'w', encoding='utf8') for path_index in
                   range(0, len(snakemake.output))]

    for i_counted, counted in enumerate(word_counts):
        counted.write(outputs[i_counted])


try:
    type(snakemake)
    main(True)
except NameError:
    print('[TEST] Executed from outside snakemake')
    main(False)
